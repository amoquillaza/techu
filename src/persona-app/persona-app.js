import {LitElement, html} from 'lit-element';
import '../persona-header/persona-header'
import '../persona-main/persona-main'
import '../persona-footer/persona-footer'
import '../persona-sidebar/persona-sidebar'
import '../persona-stats/persona-stats'

class PersonaApp extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `

            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
                <persona-main @update-people="${this.updatePeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
        `;
    }

    newPerson(e){
        console.log("newPerson en persona-app"); 
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated en persona-app", e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

    updatePeople(e){
        console.log("updatePeople en persona-app", e.detail);
        this.people = e.detail.people;
    }

    updated(changedProperties){
        console.log("updated en persona-app", changedProperties);

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }
}

customElements.define('persona-app', PersonaApp); 