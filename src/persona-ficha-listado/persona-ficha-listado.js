import {LitElement, html} from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object},
            profile: {type: String}
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div class="card h-100">
                <img class="card-img-top" heigth="50" width="50" src="${this.photo.src}" alt="${this.photo.alt}">
                <div class="card-body">
                    <h4 class="card-title">${this.name}</h4>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            ${this.yearsInCompany} años en la empresa
                        </li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>Borrar</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Info</strong></button>
                </div>
            </div>
        `;
    }

    deletePerson(e){
        console.log("deletePerson en persona-ficha-listado");
        console.log("se va a borrar a la persona de nombre " + this.name); 

        this.dispatchEvent(
            new CustomEvent("delete-person",
                {
                    detail: {
                        name: this.name
                    }
                }
            )    
        );

    }
    moreInfo(e){
        console.log("moreInfo en persona-ficha-listado");

        this.dispatchEvent(
            new CustomEvent("info-person",
                {
                    detail: {
                        name: this.name
                    }
                }
            )    
        );

    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado); 