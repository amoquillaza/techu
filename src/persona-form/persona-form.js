import {LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {

    static get properties(){
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor(){
        super();
        this.resetFormData();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input @input="${this.updateName}" type="text" id="personFormName" class="form-control" placeholder="Ingrese su nombre completo" .value="${this.person.name}" ?disabled="${this.editingPerson}" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" class="form-control" placeholder="Ingrese brevemente su perfil" rows="5" .value="${this.person.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsInCompany}" type="text" class="form-control" placeholder="Ingrese su antigüedad" .value="${this.person.yearsInCompany}" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad Name con el valor " + e.target.value);
        this.person.name = e.target.value;

    }
    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad Profile con el valor " + e.target.value);
        this.person.profile = e.target.value;

    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad YearsInCompany con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;

    }

    goBack(e){
        console.log("goBack en persona-form");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.resetFormData();
    }

    storePerson(e){
        console.log("storePerson en persona-form");
        e.preventDefault();

        this.person.photo = {
            "src": "./img/persona.jpg",
            "alt": "Persona"
        }
        console.log(this.person);

        this.dispatchEvent(new CustomEvent("persona-form-store", {detail: 
            {person: {
                name: this.person.name,
                profile: this.person.profile,
                yearsInCompany: this.person.yearsInCompany,
                photo: this.person.photo
                }, editingPerson: this.editingPerson

            }
        }));
        this.resetFormData();
    }

    resetFormData(){
        console.log("resetFormData en persona-form");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany="";
        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm); 