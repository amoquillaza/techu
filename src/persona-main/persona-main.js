import {LitElement, html, css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado';
import '../persona-form/persona-form';

class PersonaMain extends LitElement {

    static get styles(){
        return css`
            :host {
                all: initial;
            }
        `;
    }

    static get properties(){
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.showPersonForm = false; 

        this.people = [
            {profile: "Lorem ipsum dolo sit amet.",
             "name": "Selena Mora", 
             "yearsInCompany": 10,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Selena Mora"}
            },
            {profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta purus sit amet sem placerat, vitae dictum nulla lobortis. Cras placerat quis nunc sit amet ornare. In lacinia lacinia quam, ut dignissim lectus facilisis eu. Maecenas felis elit, iaculis vitae nisl sit amet, cursus tempor turpis. Sed ut egestas lectus, et iaculis diam. Nullam eget scelerisque lacus, et bibendum lacus.",
             "name": "Dolores Fuertes", 
             "yearsInCompany": 2,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Dolores Fuertes"}           
            },
            {profile: "Lorem ipsum.",
             "name": "Elena Nito del Bosque", 
             "yearsInCompany": 5,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Elena Nito del Bosque"}
            },
            {profile: "Lorem.",
             "name": "Leandro Gao", 
             "yearsInCompany": 9,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Leandro Gao"}
            },
            {profile: "Lorem ipsum dolo sit amet, consectur...",
             "name": "Arturo Moquillaza", 
             "yearsInCompany": 11,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Arturo Moquillaza"}
            },
            {profile: "Lorem ipsum dolo sit amet",
             "name": "Edgar Ballesteros", 
             "yearsInCompany": 3,
             "photo": {
                "src": "./img/persona.jpg",
                "alt": "Edgar Ballesteros"}
            }
        ];
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <h2 class="text-center">Personas</h2>
            <main>
                <div class="row text-center" id="peopleList">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.people.map(
                            person => html`<persona-ficha-listado name="${person.name}" yearsInCompany="${person.yearsInCompany}" profile="${person.profile}" .photo="${person.photo}" @delete-person="${this.deletePerson}" @info-person="${this.infoPerson}"></persona-ficha-listado>` 
                        )}
                    </div>
                </div>

                <div class="row">
                    <persona-form id="personForm" class="d-none border rounded border-primary" @persona-form-close="${this.personFormClose}" @persona-form-store="${this.personFormStore}"></persona-form>
                </div>
                        
            </main>


        `;
    }

    personFormClose(e){
        console.log("personFormClose en persona-main");
        console.log("Se ha cerrado el form de persona");
        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore en persona-main");
        console.log(e.detail.person);
        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona", e.detail.person);

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );

            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            if(indexOfPerson >= 0){
                console.log("Persona encontrada. Index: " + indexOfPerson, e.detail.person);
                this.people[indexOfPerson] = e.detail.person;
            }
        }else{
            console.log("Se va a agregar la persona " + e.detail.person.name);
            this.people.push(e.detail.person);
        }
        console.log("Persona almacenada", this.people);
        this.showPersonForm = false;

    }


    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar a la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson[0]);    
        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de showPersonForm en persona-main");
            if(this.showPersonForm === true){
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main", this.people);
            this.dispatchEvent(new CustomEvent("update-people",{
                detail:{
                    people: this.people
                }
            }));
        }
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }
}

customElements.define('persona-main', PersonaMain); 